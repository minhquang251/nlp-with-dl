# libraries for dataset preparation, feature engineering
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import model_selection, preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer
import heapq
import Logistic_Regression as lr
import copy


def classify(xtrain_tfidf, dataset_train_y, xtest_tfidf, dataset_test_y, number):
    # Change Label to fit in Binary Logistic Regression
    train_y = copy.deepcopy(dataset_train_y)
    test_y = copy.deepcopy(dataset_test_y)
    for i in range(len(train_y[0])):
        if train_y[0, i] == number:
            train_y[0, i] = 0
        else:
            train_y[0, i] = 1
    for i in range(len(test_y[0])):
        if test_y[0, i] == number:
            test_y[0, i] = 0
        else:
            test_y[0, i] = 1
    d = lr.model(xtrain_tfidf, train_y, xtest_tfidf, test_y, num_iterations = 1000, learning_rate = .75, print_cost = True)
    return d

np.random.seed(1)

# Question 1---------------------------------------------------
df = pd.read_csv('dataset.csv', encoding= 'utf-16')
#--------------------------------------------------------------

train_x, test_x, train_y, test_y = model_selection.train_test_split(df['text'], df['label'], random_state= 1, stratify= df['label'])

#print(train_x.head())
#print(train_y.head())

# Question 2 - word level tf-idf ------------------------------------------------
tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', max_features=5000, encoding='utf-16')

tfidf_vect.fit(train_x)
tfidf_vect.fit(test_x)
xtrain_tfidf = tfidf_vect.transform(train_x)
xtest_tfidf = tfidf_vect.transform(test_x)
#--------------------------------------------------------------------------------

# label encode the target variable, encode labels to 0, 1, or 2
encoder = preprocessing.LabelEncoder()
train_y = encoder.fit_transform(train_y)
test_y = encoder.fit_transform(test_y)
#print(encoder.transform(['XH', 'DS', 'KH']))

# Getting transformed training and testing dataset
print('Number of training documents: %s' %str(xtrain_tfidf.shape[0]))
print('Number of testing documents: %s' %str(xtest_tfidf.shape[0]))
print('Number of features of each document: %s' %str(xtrain_tfidf.shape[1]))
#print('xtrain_tfidf shape: %s' %str(xtrain_tfidf.shape))
#print('train_y shape: %s' %str(train_y.shape))
#print('xtest_tfidf shape: %s' %str(xtest_tfidf.shape))
#print('test_y shape: %s' %str(test_y.shape))

# Transpose for Convenience
train_y = np.expand_dims(train_y, axis=0)
test_y = np.expand_dims(test_y, axis=0)

# for convenience in this exercise, we also use toarray() to convert sparse to dense matrix 
xtrain_tfidf =  xtrain_tfidf.T.toarray() 
xtest_tfidf =  xtest_tfidf.T.toarray()

# New shape 
print('xtrain_tfidf shape: %s' %str(xtrain_tfidf.shape))
print('train_y shape: %s' %str(train_y.shape))
print('xtest_tfidf shape: %s' %str(xtest_tfidf.shape))
print('test_y shape: %s' %str(test_y.shape))

# Question 3 - Find the most important 10 words in each article and each topic--------------
# The most important words in each article (Uncomment to run)
'''
dict = tfidf_vect.vocabulary_ # Shape {word : index}

for i in range(xtrain_tfidf.shape[1]):
    label = encoder.inverse_transform([train_y[0, i]])
    priorityQueue = []
    for word in dict:
        score = xtrain_tfidf[dict[word], i]
        heapq.heappush(priorityQueue, (-score, word))
    top10 = heapq.nsmallest(10, priorityQueue)
    wordList = [top10[i][1] for i in range(10)]
    print(label, wordList)
'''
# The most important words in each topic (Uncomment to run)
'''
topic_df = pd.read_csv('topic_dataset.csv', encoding= 'utf-16')
topic_tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', max_features=5000, encoding='utf-16')
topic_tfidf_vect.fit(topic_df['text'])
topic_xtrain_tfidf = topic_tfidf_vect.transform(topic_df['text'])
encoder = preprocessing.LabelEncoder()
topic_train_y = encoder.fit_transform(topic_df['label'])
topic_train_y = np.expand_dims(topic_train_y, axis=0)
topic_xtrain_tfidf =  topic_xtrain_tfidf.T.toarray()
topic_dict = topic_tfidf_vect.vocabulary_
for i in range(topic_xtrain_tfidf.shape[1]):
    label = encoder.inverse_transform([topic_train_y[0, i]])
    priorityQueue = []
    for word in topic_dict:
        score = topic_xtrain_tfidf[topic_dict[word], i]
        heapq.heappush(priorityQueue, (-score, word))
    top10 = heapq.nsmallest(10, priorityQueue)
    wordList = [top10[i][1] for i in range(10)]
    print(label, wordList)
'''
# ------------------------------------------------------------------------------------------

# Question 4 - Logistic Regression ---------------------------------------------------------
# Classify 0 and ~0:
d0 = classify(xtrain_tfidf, train_y, xtest_tfidf, test_y, 0)
# Classify 1 and ~1:
d1 = classify(xtrain_tfidf, train_y, xtest_tfidf, test_y, 1)
# Classify 2 and ~2:
d2 = classify(xtrain_tfidf, train_y, xtest_tfidf, test_y, 2)

Y_train_prediction = np.zeros((1,xtrain_tfidf.shape[1]), dtype=np.int)
Y_test_prediction = np.zeros((1,xtest_tfidf.shape[1]), dtype=np.int)

for i in range(Y_train_prediction.shape[1]):
    Y_train_prediction[0, i] = np.argmin(np.array([d0['sigmoid_result_train'][0, i],
    d1['sigmoid_result_train'][0, i],
    d2['sigmoid_result_train'][0, i]]))
for i in range(Y_test_prediction.shape[1]):
    Y_test_prediction[0, i] = np.argmin(np.array([d0['sigmoid_result_test'][0, i],
    d1['sigmoid_result_test'][0, i],
    d2['sigmoid_result_test'][0, i]]))

print("Final Train Accuracy: {} %".format(100 - np.mean(np.abs(Y_train_prediction - train_y)) * 100))
print("Final Test Accuracy: {} %".format(100 - np.mean(np.abs(Y_test_prediction - test_y)) * 100))
# ------------------------------------------------------------------------------------------